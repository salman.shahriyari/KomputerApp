//referencing HTML Elements

const balanceElement = document.getElementById("balance");
const outbalanceElement = document.getElementById("outbalance");
const payElement = document.getElementById("pay");
const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features")
const modelELement = document.getElementById("model");
const descElement = document.getElementById("desc");
const priceElement = document.getElementById("price");
const workButtonElement = document.getElementById("workbtn");
const bankButtonElement = document.getElementById("bankbtn");
const getaloanButtonElement = document.getElementById("getaloanbtn");
const buynowButtonElement = document.getElementById("buynowbtn");
const outdivElement = document.getElementById("outdiv");
const repayloanElement = document.getElementById("repayloan")
const picElement = document.getElementById("pic")


//variables to work with
let laptops = [];

//fetching API Values
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsItems(laptops))


//initializing laptops Items
const addLaptopsItems = (laptops) => {
    laptops.forEach(x => addLaptopItems(x));
    priceElement.innerText = laptops[0].price;
    descElement.innerText = laptops[0].description;
    modelELement.innerText = laptops[0].title;
    // featuresElement.innerText = laptops[0].specs;
    picElement.src= "https://noroff-komputer-store-api.herokuapp.com/"+laptops[0].image;
    console.log(laptops[0].image)
    featuresElement.innerText = "";
    // let items = selectedLaptop.specs;
    // console.log(items)
    let items = laptops[0].specs;

    ul = document.createElement('ul');

    featuresElement.appendChild(ul);

    items.forEach(function (item) {
        let li = document.createElement('li');
        ul.appendChild(li);

        li.innerHTML += item;
    });
    }

//handling changes in laptop
const addLaptopItems = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
    }

//functions for eventhandlers
const handleLaptopItemsChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = selectedLaptop.price;
    modelELement.innerHTML = selectedLaptop.title;
    descElement.innerHTML = selectedLaptop.description;
    picElement.src= "https://noroff-komputer-store-api.herokuapp.com/"+selectedLaptop.image;
    featuresElement.innerText = "";
    // let items = selectedLaptop.specs;
    // console.log(items)
    let items = selectedLaptop.specs;

    ul = document.createElement('ul');

    featuresElement.appendChild(ul);

    items.forEach(function (item) {
        let li = document.createElement('li');
        ul.appendChild(li);

        li.innerHTML += item;
    });
    //  featuresElement.innerText = selectedLaptop.specs;

    }

const handleWork = () => {
    currentPay = parseFloat(payElement.innerText)
    // const totalPaid = prompt("Please enter the amount you wish to pay: ");
    // const change = parseFloat(totalPaid) - totalDue;
    // alert(`Total change due: ${change.toFixed(2)}`);
    payElement.innerText = currentPay+100
}

const handleBank = () => {
    currentPay = parseFloat(payElement.innerText)
    currentbalance = parseFloat(balanceElement.innerText)
    currentoutbalance = parseFloat(outbalanceElement.innerText)

    // console.log(currentPay)
    // console.log(currentbalance)
    // console.log(currentoutbalance)
    
    //console.log(currentPay)
    if(parseFloat(outbalanceElement.innerText)>0) {
        currentoutbalance -= (0.1 * currentPay);
        currentbalance += (0.9 * currentPay);
        balanceElement.innerText = currentbalance;
        outbalanceElement.innerText = currentoutbalance;
    }
    else {
        currentbalance += currentPay;
        balanceElement.innerText = currentbalance;
    }
    payElement.innerText = 0;
}

const handleLoan = () => {
    currentbalance = parseFloat(balanceElement.innerText)
    currentoutbalance = parseFloat(outbalanceElement.innerText)

    // console.log(currentbalance)
    // console.log(currentoutbalance)

    if (currentoutbalance >0){
        
        alert(`first pay your previous loan`);
    }

    else {
        let requestedLoan = prompt("Please enter the amount you wish to loan: ");
        requestedLoan = parseFloat(requestedLoan);
        if(requestedLoan > currentbalance * 2){

            alert(`Out of limit for your loan !!`);
        }
        else {

            outdivElement.style.visibility = "visible";
            repayloanElement.style.visibility = "visible";
            currentbalance += parseFloat(requestedLoan);
            //currentoutbalance = parseFloat(balanceElement.innerText)
            currentoutbalance = requestedLoan;
    
            balanceElement.innerText = currentbalance;
            outbalanceElement.innerText = currentoutbalance;
            
    
        }

    }


}

const handleRepay = () => {
    currentbalance = parseFloat(balanceElement.innerText)
    currentoutbalance = parseFloat(outbalanceElement.innerText)
    currentpay = parseFloat(payElement.innerText)
    console.log(payElement.innerText)
    if (payElement.innerText == "0") {
        alert("Dude! You need to work to earn some money !")
    }

    else {
        // outbalanceElement.innerText -= parseFloat(payElement.innerText)
        
        newbalance = outbalanceElement.innerText - payElement.innerText
        console.log(newbalance)
        if(newbalance <= 0){
            outbalanceElement.innerText = 0;
            balanceElement.innerText = parseFloat(balanceElement.innerText) - newbalance
            repayloanElement.style.visibility = "hidden";
            outdivElement.style.visibility = "hidden";

        }
        else {
            outbalanceElement.innerText = newbalance
        }
        payElement.innerText = 0 ;
    }

}

const handleBuy = () => {
    currentbalance = parseFloat(balanceElement.innerText)
    price = parseFloat(priceElement.innerText);

    if(price>currentbalance)  {
        alert("You don't have enough money")
    }
    else {
        balanceElement.innerText = currentbalance-price;
        alert("You own the laptop")
    }
}


//list of event listeners
laptopsElement.addEventListener("change", handleLaptopItemsChange);
workButtonElement.addEventListener("click", handleWork);
bankButtonElement.addEventListener("click", handleBank);
getaloanButtonElement.addEventListener("click", handleLoan)
buynowButtonElement.addEventListener("click", handleBuy)
repayloanElement.addEventListener("click",handleRepay)


