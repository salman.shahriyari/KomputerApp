# KomputerApp

KomputerApp is a simple app written by vanilla JS which simulates buying a computer using your current balance. It has also options to add your earning and get loan from bank.


## Getting Started


### Executing program

* Download the program code and run it in your local machine. 
* Then you are to make your required changes.

## Usage

* The Work button adds your current earninh to your current total pay. Each click is equal to 100 kroner.

* The Bank Button transfers your pat to your current balance. If you have an outstanding loan, 10% of your salary MUST first be deducted and transferred to the outstanding Loan amount. Then, The balance after the 10% deduction may be transferred to your bank account

* The Get a loan button will attempt to get a loan from the bank. When the Get a loan button is clicked, it shows a “Prompt” popup box that allows you to enter an amount.you are only allowed to get a loan up to twice your current balance, and if you have not paid your previous loan, you are not allowed to get loan.

* The Repay Loan appears only if you have a loan. you could pay your loan form you current pay amount.

* Laptop selection allows you to choose a laptop model and see its specs, image and description.

* The Buy Now part allows you to buy the selected laptop if you have enough amount in your balance.


## Contributing
It is a demo app and you are welcome to give your opinions to make the app features and its visualisation better


## Authors

* Salman Shahriyari  
* [@salman.shajriyari]

## Version History

* 0.1
    * Initial Release

## Acknowledgments and inspiration

* Using Dewald Els Drink App project as inspiration
* part of Noroff School Front-End Course
